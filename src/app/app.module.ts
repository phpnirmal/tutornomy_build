import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
 import { HttpClientModule, HttpClient } from '@angular/common/http';

import { MyApp } from './app.component';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { UserServiceProvider } from '../providers/user-service/user-service';
import { ValidationProvider } from '../providers/validation/validation';
import { NativeStorage } from '@ionic-native/native-storage';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { TopupPlansProvider } from '../providers/topup-plans/topup-plans';
import { RequestProvider } from '../providers/request/request';
import { AccountProvider } from '../providers/account/account';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    UserServiceProvider,
    ValidationProvider,
    LocalStorageProvider,
    TopupPlansProvider,
    RequestProvider,
    AccountProvider
  ]
})
export class AppModule {}
