let basePathUrl = 'http://23.239.16.58:9569/';

export var config: any = {
	BasePath : basePathUrl,
	UserSignup:basePathUrl+'users',
	Login:basePathUrl+'auth',
	create_student_profile:basePathUrl+'studentProfiles',
	update_student_profile:basePathUrl+'	studentProfiles/',
	create_tutor_profile:basePathUrl+'tutorProfiles',
	getStudentProfile:basePathUrl+'studentProfiles/',
	get_tutor_profile:basePathUrl+'tutorProfiles/',
	get_current_user:basePathUrl+'users/me',
	get_topup_plans:basePathUrl+'topupPlans',
	create_topup_plan:basePathUrl+'topupPlans',
	get_subjects:basePathUrl+'subjects',
	get_academic_levels:basePathUrl+'grades',
	create_student_request:basePathUrl+'studentRequests',
	get_student_requests:basePathUrl+'studentRequests/studentRequestList/',
	get_account_data:basePathUrl+'accountBalCps/',
	get_transaction_details:basePathUrl+'transLedgers/'
};

