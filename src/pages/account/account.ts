import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import {AccountProvider } from '../../providers/account/account';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
	public userId;
      public shownGroup = null;
      public shownGroupA=null;
      public shownGroupB=null;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public localStore:LocalStorageProvider,public accSer:AccountProvider,
      public userSer:UserServiceProvider) {
  	this.getUserId();
  }
  ionViewWillEnter(){
  	this.getAccBalance();
      this.getBillingdetails();
      this.getTransactions();
  }
  getUserId(){
    this.localStore.getvalue('userid')  
    .then((id)=>{
      this.userId=id;
      this.localStore.setvalue('userid',this.userId);
      console.log(id,'IDDDSSS');
    })
  }
  getAccBalance(){
	this.accSer.getAccData(this.userId)
		.subscribe((res:any)=>{
		console.log(res);
	},Error=>{
		console.log(Error);
	})
  }
  getBillingdetails(){
    this.userSer.getprofileData(this.userId)
      .subscribe((res)=>{
        console.log(res,'profile');
      },error=>{
        console.log(error,'errror');
      })
  }
  getTransactions(){
    this.accSer.getTransaction(this.userId)
          .subscribe((res)=>{
            console.log(res);
          },error=>{
            console.log(error);
          })
  }
toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
};
isGroupShown(group) {
    return this.shownGroup === group;
};
toggleGroupA(group) {
    if (this.isGroupShownA(group)) {
        this.shownGroupA = null;
    } else {
        this.shownGroupA = group;
    }
};
isGroupShownA(group) {
    return this.shownGroupA === group;
};
toggleGroupB(group) {
    if (this.isGroupShownB(group)) {
        this.shownGroupB = null;
    } else {
        this.shownGroupB = group;
    }
};
isGroupShownB(group) {
    return this.shownGroupB === group;
};

}
