import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoosePackagePage } from './choose-package';

@NgModule({
  declarations: [
    ChoosePackagePage,
  ],
  imports: [
    IonicPageModule.forChild(ChoosePackagePage),
  ],
})
export class ChoosePackagePageModule {}
