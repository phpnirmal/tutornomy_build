import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController} from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { TopupPlansProvider } from '../../providers/topup-plans/topup-plans';

@IonicPage()
@Component({
  selector: 'page-choose-package',
  templateUrl: 'choose-package.html',
})
export class ChoosePackagePage {  
    public id;
    public token;
    public plans;
    public plan_type;
    public trail_color:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public localStore:LocalStorageProvider,public userSer:UserServiceProvider,
    public topupSer:TopupPlansProvider,public toastCtrl:ToastController,
    public loadingCtrl:LoadingController) {
  	this.getUserId();
    this.getToken();
    this.getAllPackages();
   
  }
  getAllPackages(){
      const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  	this.topupSer.getStudentPackages()
  	.subscribe((res:any)=>{
        this.plans=res.rows;
        loader.dismiss();
        console.log(this.plans,'allPackages');
  	},error=>{
      console.log(error);
       loader.dismiss();
    })
  }
    getUserId(){
    this.localStore.getvalue('userid')
    .then((id)=>{
      this.id=id;
      console.log(id,'IDDDSSS');
    })
  }
  getToken(){
    this.localStore.getvalue('token')
    .then((token)=>{
      console.log(token,'choose token');
      this.token=token;
      this.localStore.setvalue('token',this.token);
    })
  }
  selectPlan(idx,plan){
    console.log(idx,plan);
      this.trail_color = '#436fbb';
      this.plan_type=plan;       
  }
  savePlan(plan){
    if(this.plan_type){
       this.navCtrl.push('BillingAddressPage');
    }else{
        let toast = this.toastCtrl.create({
    message: 'please choose a package',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
    }
  }
}
