import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController} from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-billing-address',
  templateUrl: 'billing-address.html',
})
export class BillingAddressPage {
	public id;
	public user_id;
	public updatestudentform : FormGroup;
       public cardsColor: string;
       public netColor: string;
       public paypalColor: string;
       public cards: boolean=true;
       public net:boolean=true;
       public paypal:boolean=true;
       public card_white:string;
      public net_white:string;
      public paypal_white:string;
      public payment_method:Array<any>=[];
      public card_color:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public localStore:LocalStorageProvider,public userSer:UserServiceProvider,
  	public fb:FormBuilder,public toastCtrl:ToastController) {
  	this.getprofileId();
  	this.getUserId();
  	    this.updatestudentform=this.fb.group({
            	address1: ['',Validators.required],
            	street: ['',Validators.required],
            	city: ['',Validators.required],
            	zip: ['',Validators.required],
	});
        this.payment_method=[
              {title:'Card Payment',pimage:'assets/icon/card-icon.png'},
              {title:'Net Banking',pimage:'assets/icon/bank-icon.png'},
              {title:'Paypal Payment',pimage:'assets/icon/paypal.png'},
        ]
  }
  getUserId(){
    this.localStore.getvalue('profileid')
    .then((id)=>{
      this.user_id=id;
      console.log(id,'ID1');
    })
  }
   getprofileId(){
    this.localStore.getvalue('id')
    .then((id)=>{
      this.id=id;
      console.log(id,'ID2');
    })
  }
  updateForm(){
  	console.log(this.updatestudentform.value);
  	if(this.updatestudentform.valid){
  		this.userSer.updateStudentProfile({userId:this.id,
  			address1:this.updatestudentform.value.address1
  			,street:this.updatestudentform.value.street
  			,city:this.updatestudentform.value.city
  			,zip:this.updatestudentform.value.zip},this.user_id)
  		.subscribe((res)=>{
  			console.log(res);
  			this.navCtrl.push('LoginPage');
  		},error=>{
  			console.log(error);
  			this.navCtrl.push('LoginPage');
  		})
  		
  	}else{
       let toast = this.toastCtrl.create({
    message: 'All fields are required',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
    }
  }
selectPayment(){
   this.card_color = '#436fbb';
}
}
