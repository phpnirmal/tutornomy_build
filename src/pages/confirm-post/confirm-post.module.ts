import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPostPage } from './confirm-post';

@NgModule({
  declarations: [
    ConfirmPostPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPostPage),
  ],
})
export class ConfirmPostPageModule {}
