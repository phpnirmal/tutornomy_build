import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { ValidationProvider } from '../../providers/validation/validation';
import { NativeStorage } from '@ionic-native/native-storage';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';


@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
public registerform : FormGroup;
public access_token;
public userId;
public id;
public me;
 public avaImage: string = 'n_button';
  public avbImage: string = 'n_button';
   public avcImage: string = 'n_button';
   public avdImage: string = 'n_button';
  constructor(public navCtrl: NavController, public navParams: NavParams,
  		public fb:FormBuilder,public userSer:UserServiceProvider,
  		public validation:ValidationProvider,public toastCtrl: ToastController,
            public nativeStorage: NativeStorage,public localStore:LocalStorageProvider) {

    this.registerform=this.fb.group({
            role: ['student',Validators.required],
            // avatar: ['',Validators.required],
            email:['akon@gmail.com',Validators.compose([validation.emailValidation,Validators.required])],
            // avatarIm: ['',Validators.required],
            // type: ['',Validators.required],
            // otp: ['',Validators.required],
            // otpExp: ['',Validators.required],
            // ftLogin: ['',Validators.required],
            // status: ['',Validators.required],
            currency: ['',Validators.required],
            // deviceId: ['',Validators.required],
            name: ['',Validators.required],
            // first:['',Validators.required],
            // last:['',Validators.required],
            // confirm:['',Validators.required],  
            password: ['123456', Validators.required],
            mobile:['9855458514',Validators.required],
            // access_token:['',Validators.required]
        });
  }
  registerForm(){
    // this.createUserProfile();
    if(this.registerform.value.role==""&&this.registerform.value.email==""&&
      this.registerform.value.name==""&&this.registerform.value.last==""&&
      this.registerform.value.password==""&&this.registerform.value.confirm==""&&
      this.registerform.value.mobile==""&&this.registerform.value.avatar=="")
    {
      let toast = this.toastCtrl.create({
    message: 'All fields are required',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
  }else if(this.registerform.value.password!=this.registerform.value.currency){
      let toast = this.toastCtrl.create({
    message: 'Password mismatch',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
  }else if(this.registerform.valid){
    this.userSer.userSignup(this.registerform.value)
    .subscribe((res:any)=>{
      console.log(res);
      this.access_token=res.token;
      this.userId=res.user.email;
      console.log(this.userId);
      this.createUserProfile();
      this.setValue();
      console.log(this.access_token,'token');
      this.navCtrl.push('ChoosePackagePage');
    },Error=>{
      console.log(Error);
    })
  }else{
     let toast = this.toastCtrl.create({
    message: 'All fields are required',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
  }
}
  setValue(){
    // this.nativeStorage.setItem('token',this.access_token);
    this.localStore.setvalue('token',this.access_token);
  }
  createUserProfile(){
      if(this.registerform.value.role=='student'){
        this.userSer.createStudentProfile({fullname:this.registerform.value.name,access_token:this.access_token,
                userId:this.userId,pMobile:this.registerform.value.mobile})
            .subscribe((res:any)=>{
                console.log(res,'student profile');
                this.id=res.user.email;
                this.localStore.setvalue('id',this.id);  
                this.localStore.setvalue('profileid',res.id);
                this.me=res.user.role;  
            },error=>{
                console.log(error,'studerorr');
            })
      }else{
        this.userSer.createTutorProfile({fullname:this.registerform.value.name,access_token:this.access_token,
                userId:this.userId,pMobile:this.registerform.value.mobile})
                .subscribe((res:any)=>{
                  console.log(res,'tutor profile');
                  this.id=res.user.email;
                  this.localStore.setvalue('id',this.id);
                    this.me=res.user.role;
                },error=>{
                  console.log(error,'tuterror');
                })
        console.log('admin');
      }
  }
  av1(){
    console.log('works');
   this.avaImage = 'v_buttonclik';
    this.avbImage='n_button';
        this.avcImage='n_button';
       this.avdImage='n_button';
  }
  av2(){
     this.avbImage='v_buttonclik';
        this.avcImage='n_button';
       this.avdImage='n_button';
     this.avaImage = 'n_button';
  }
  av3(){
    this.avcImage='v_buttonclik';
       this.avdImage='n_button';
     this.avaImage = 'n_button';
     this.avbImage='n_button';
  }
  cam(){
     this.avdImage='v_buttonclik';
     this.avaImage = 'n_button';
     this.avbImage='n_button';
     this.avcImage='n_button';
  }
}