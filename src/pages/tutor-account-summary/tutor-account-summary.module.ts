import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorAccountSummaryPage } from './tutor-account-summary';

@NgModule({
  declarations: [
    TutorAccountSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorAccountSummaryPage),
  ],
})
export class TutorAccountSummaryPageModule {}
