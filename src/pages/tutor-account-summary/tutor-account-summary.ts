import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutor-account-summary',
  templateUrl: 'tutor-account-summary.html',
})
export class TutorAccountSummaryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
