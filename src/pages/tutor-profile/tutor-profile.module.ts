import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorProfilePage } from './tutor-profile';

@NgModule({
  declarations: [
    TutorProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(TutorProfilePage),
  ],
})
export class TutorProfilePageModule {}
