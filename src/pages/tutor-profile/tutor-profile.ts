import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController} from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { UserServiceProvider } from '../../providers/user-service/user-service';


@IonicPage()
@Component({
  selector: 'page-tutor-profile',
  templateUrl: 'tutor-profile.html',
})
export class TutorProfilePage {
	public profile_id;
 	public access_token;
     public profile_data;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public localStore:LocalStorageProvider,public userSer:UserServiceProvider,
    public loadingCtrl:LoadingController) {
    setTimeout(()=>{
        this.getToken();
    },3000)
    this.localStore.getvalue('userid')
    .then((id)=>{
      this.profile_id=id;
      if(this.profile_id){
          this.getTutorProfie();
      }
      console.log(id,'IDDDSSS');
    })
    this.getMe();
  }
  ionViewWillEnter(){
    this.getTutorProfie();
  }
  getTutorProfie(){
      const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
	this.userSer.retrieveTutorProfile(this.profile_id)
	.subscribe((res:any)=>{
	console.log(res,'TPROFILE');
    this.profile_data=res;
        loader.dismiss();
	},error=>{
	console.log(error);
    loader.dismiss();
	})
  }
    getTutorId(){
    this.localStore.getvalue('userid')
    .then((id)=>{
      this.profile_id=id;
      console.log(id,'IDDDSSS');
    })
  }
getToken(){
    this.localStore.getvalue('token')
    .then((token)=>{
      this.access_token=token;
      console.log(token,'TOKENss');
    })
  }
  getMe(){
    this.userSer.getCurrentUser().
      subscribe((res:any)=>{
          console.log(res,'cool');
      },error=>{
        console.log(error);
      })
  }
}
