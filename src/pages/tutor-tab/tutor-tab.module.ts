import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorTabPage } from './tutor-tab';

@NgModule({
  declarations: [
    TutorTabPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorTabPage),
  ],
})
export class TutorTabPageModule {}
