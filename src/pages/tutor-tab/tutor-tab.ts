import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutor-tab',
  templateUrl: 'tutor-tab.html',
})
export class TutorTabPage {
	tab1Root = 'TutorAccountPage';	
	tab2Root = 'TutorAccountSummaryPage';
	tab3Root = 'TutorDashboardPage';
	tab4Root ='TutorProfilePage';
	tab5Root='TutorProfileSettingsPage';
	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}
}
