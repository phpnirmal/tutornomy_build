import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorStartSessionPage } from './tutor-start-session';

@NgModule({
  declarations: [
    TutorStartSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorStartSessionPage),
  ],
})
export class TutorStartSessionPageModule {}
