import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutor-start-session',
  templateUrl: 'tutor-start-session.html',
})
export class TutorStartSessionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  		public viewCtrl: ViewController) {
  }
  dismissView(){
  	this.viewCtrl.dismiss();
  }
}
