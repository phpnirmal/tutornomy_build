import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tutor-profile-settings',
  templateUrl: 'tutor-profile-settings.html',
})
export class TutorProfileSettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
