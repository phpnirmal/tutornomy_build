import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorProfileSettingsPage } from './tutor-profile-settings';

@NgModule({
  declarations: [
    TutorProfileSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorProfileSettingsPage),
  ],
})
export class TutorProfileSettingsPageModule {}
