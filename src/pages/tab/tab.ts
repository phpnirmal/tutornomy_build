import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {
	public user_id;
tab1Root = 'AccountPage';	
tab2Root = 'StudentReportPage';
tab3Root = 'HomePage';
tab4Root ='PostQuestionsPage';
tab5Root='ProfileSettingsPage';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.user_id=this.navParams.get('userid');
  	console.log(this.user_id);
  }
}
