import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController,App} from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { RequestProvider } from '../../providers/request/request';

@IonicPage()
@Component({
  selector: 'page-post-questions',
  templateUrl: 'post-questions.html',
})
export class PostQuestionsPage {
        // public  buttonColor: string = "light";
	  public access_token;
	  public subjects;
	  public academics;
	  public userId;
	   public active_category:boolean=false;
	   public sub_name;
	   public note: string;
	   public subjectbg:string='#ffffff';
	   public subjectcolor:string='#000000';
         public user_data;
         public academiclevel;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public userSer:UserServiceProvider,public localStore:LocalStorageProvider,
  	public toastCtrl:ToastController,public requestSer:RequestProvider,
  	public loadingCtrl: LoadingController,public app:App) {


  	 setTimeout(()=>{
      			this.getToken();
      			this.getUserId();
           	},2000);
  }
  ionViewWillEnter(){
  	this.getSubjects();
  	this.getAcademicLevel();
       this.getuserProfile();
  }
  getSubjects(){
	let loader = this.loadingCtrl.create({
	content: "Please wait...",
	duration: 3000
	});
	loader.present();
  	this.userSer.getSubjects()
  	.subscribe((res:any)=>{
  		console.log(res,'hhhh');
  		this.subjects=res;
  		loader.dismiss();
  	},error=>{
  		console.log(error);
  		loader.dismiss();
  	})
  }
  getAcademicLevel(){
  	this.userSer.getAcademics()
  	.subscribe((res:any)=>{
  		console.log(res,'success');
  		this.academics=res;
  	},error=>{
  		console.log(error,'error');
  	})
  }
   getToken(){
    this.localStore.getvalue('token')
    .then((token)=>{
      this.access_token=token;
      console.log(token,'ATOKEN');
    })
  }
  getUserId(){
    this.localStore.getvalue('userid')  
    .then((id)=>{
      this.userId=id;
      console.log(id,'userID');
    })
  }
  getSub(subname,i){
      // this.buttonColor = "primary";
        for(let a of this.subjects.rows){
       a.selected = false;
     }
      // this.subjects.rows[i].selected = true;
       subname.selected = !subname.selected;
  	console.log(i,'index');
  	this.sub_name=subname;	
  }
  getAcademic(alevel){
        for(let b of this.academics.rows){
       b.selected = false;
     }
      alevel.selected = !alevel.selected;
    this.academiclevel=alevel;
  }
  postQuestion(){
    console.log('1');
  	if(this.access_token && this.userId &&this.sub_name&&this.note){
  		this.requestSer.submitQuestion(
  			{access_token:this.access_token,
  			userId:this.userId,stdQuery:this.note,
  			transId:this.sub_name
  			})
  		.subscribe((res:any)=>{
  			console.log(res,'Qsubmitted');
  			this.app.getRootNav().push('ConfirmPostPage');
  		},error=>{
  			console.log(error,'Qerror');
  		})
  	}else if(!this.sub_name){
		let toast = this.toastCtrl.create({
			message: 'Please choose subject',
			duration: 3000,
			position: 'bottom'
		});
		toast.present();
  	}else if(!this.note){
		let toast = this.toastCtrl.create({
		message: 'Please write your question',
		duration: 3000,
		position: 'bottom'
		});
		toast.present();
  	}else if(!this.access_token || this.userId)
  	{
  		let toast = this.toastCtrl.create({
		message: 'your token expired',
		duration: 3000,
		position: 'bottom'
		});
		toast.present();
  	}
  }
  getuserProfile(){
    this.userSer.getprofileData(this.userId)
    .subscribe((res:any)=>{
      this.user_data=res;
      console.log(res,'result');
    },error=>{
      console.log(error,'error');
    })
  }
}
