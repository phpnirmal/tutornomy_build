import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostQuestionsPage } from './post-questions';

@NgModule({
  declarations: [
    PostQuestionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PostQuestionsPage),
  ],
})
export class PostQuestionsPageModule {}
