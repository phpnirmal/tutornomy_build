import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishSessionPage } from './finish-session';

@NgModule({
  declarations: [
    FinishSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishSessionPage),
  ],
})
export class FinishSessionPageModule {}
