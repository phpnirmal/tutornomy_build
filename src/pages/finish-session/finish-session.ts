import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-finish-session',
  templateUrl: 'finish-session.html',
})
export class FinishSessionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
