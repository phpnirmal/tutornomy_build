import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorAcceptPage } from './tutor-accept';

@NgModule({
  declarations: [
    TutorAcceptPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorAcceptPage),
  ],
})
export class TutorAcceptPageModule {}
