import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App,ViewController} from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tutor-accept',
  templateUrl: 'tutor-accept.html',
})
export class TutorAcceptPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  		public app:App,public viewCtrl:ViewController) {
  }
  acceptRequest(){
  	this.app.getRootNav().push('TutorStartSessionPage');
  }
  dismissView(){
  	this.viewCtrl.dismiss();
  }
}
