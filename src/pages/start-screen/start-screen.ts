import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-start-screen',
  templateUrl: 'start-screen.html',
})
export class StartScreenPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  navToSignup(){
  	this.navCtrl.push('SignUpPage');
  }
  navToSignin(){
  	this.navCtrl.push('LoginPage');
  }
}
