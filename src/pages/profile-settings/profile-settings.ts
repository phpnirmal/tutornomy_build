import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-profile-settings',
  templateUrl: 'profile-settings.html',
})
export class ProfileSettingsPage {
	public tab_param;
	public user_data;
  public access_token;
  public id;
  constructor(public navCtrl: NavController, public navParams: NavParams,
		public userSer:UserServiceProvider,public localStore:LocalStorageProvider) {
  	 this.tab_param=this.navParams.data;
        // setTimeout(()=>{
          // this.getUserId();
          this.getToken();
      // },3000);

    this.localStore.getvalue('userid')  
    .then((id)=>{
      this.id=id;
       console.log(this.id,'ID');
         })
    this.getMe();
  }
  ionViewWillEnter(){  
    this.getuserProfile();
  }
  getuserProfile(){
  	this.userSer.getprofileData(this.id)
  	.subscribe((res:any)=>{
  		this.user_data=res;
  		console.log(res,'result');
  	},error=>{
  		console.log(error,'error');
  	})
  }
  getToken(){
    this.localStore.getvalue('token')
    .then((token)=>{
      this.access_token=token;
      console.log(token,'TOKENss');
    })
  }
  getUserId(){
    this.localStore.getvalue('userid')  
    .then((id)=>{
      this.id=id;
      console.log(id,'IDDDSSS');
    })
  }
   getMe(){
    this.userSer.getCurrentUser().
      subscribe((res:any)=>{
          console.log(res,'cool');
      },error=>{
        console.log(error);
      })
  }
}
