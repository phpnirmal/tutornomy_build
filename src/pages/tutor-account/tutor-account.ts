import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutor-account',
  templateUrl: 'tutor-account.html',
})
export class TutorAccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
