import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorAccountPage } from './tutor-account';

@NgModule({
  declarations: [
    TutorAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorAccountPage),
  ],
})
export class TutorAccountPageModule {}
