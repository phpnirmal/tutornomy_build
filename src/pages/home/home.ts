import { Component } from '@angular/core';
import { NavController,LoadingController} from 'ionic-angular';
import { IonicPage } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import {RequestProvider} from '../../providers/request/request';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public poststatus;
	public userId;
  constructor(public navCtrl: NavController,public loadingCtrl: LoadingController,
  	public localStore:LocalStorageProvider,public requestSer:RequestProvider) {
  	this.poststatus='requesting';
  	this.getUserId();
	  }
  ionViewWillEnter(){
  	this.getAllrequests();
  }
  navToQuerypost(){
 	this.navCtrl.push('PostQuestionsPage');
  }
getUserId(){
    this.localStore.getvalue('userid')  
    .then((id)=>{
      this.userId=id;
      this.localStore.setvalue('userid',this.userId);
      console.log(id,'IDDDSSS');
    })
  }
  getAllrequests(){
  	this.requestSer.getBackRequests(this.userId)
  		.subscribe((res:any)=>{
  			console.log(res);
  		},error=>{
  			console.log(error);
  		})
  }
}
