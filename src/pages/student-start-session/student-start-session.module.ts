import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentStartSessionPage } from './student-start-session';

@NgModule({
  declarations: [
    StudentStartSessionPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentStartSessionPage),
  ],
})
export class StudentStartSessionPageModule {}
