import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-student-start-session',
  templateUrl: 'student-start-session.html',
})
export class StudentStartSessionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
