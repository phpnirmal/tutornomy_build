import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorDashboardPage } from './tutor-dashboard';

@NgModule({
  declarations: [
    TutorDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorDashboardPage),
  ],
})
export class TutorDashboardPageModule {}
