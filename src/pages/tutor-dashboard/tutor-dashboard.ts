import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App} from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-tutor-dashboard',
  templateUrl: 'tutor-dashboard.html',
})
export class TutorDashboardPage {
	public poststatus;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  	public app:App) {
  	this.poststatus='requesting';
  }
  navToConfirm(){
  	this.app.getRootNav().push('TutorAcceptPage');
  }
}
