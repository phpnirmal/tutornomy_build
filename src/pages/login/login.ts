import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { NativeStorage } from '@ionic-native/native-storage';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginform : FormGroup;
  public access_token;
  public user_id;
  public user_type;
  public id;
  constructor(public navCtrl: NavController, public navParams: NavParams,
                    public formBulder:FormBuilder,public userSer:UserServiceProvider,
                    public nativeStorage: NativeStorage,public toastCtrl: ToastController,
                    public localStore:LocalStorageProvider) {
        this.getValue();
        this.localStore.delete('token');
       this.loginform=this.formBulder.group({
                email: ['',Validators.required],
                password:['',Validators.required],
               // access_token:['',Validators.required]
            });
  }
  navToSignup(){
  	this.navCtrl.push('SignUpPage');
  }
  navToTabpage(){
  	this.navCtrl.push('TabPage');
  }
  logForm(){
    console.log(this.loginform.value);
    if(this.loginform.value.email!=""&&this.loginform.value.password!="")
    {
    this.userSer.userLogin(this.loginform.value.email,this.loginform.value.password)
      .subscribe((res:any)=>{
        this.access_token=res.token;
        if(res.token){
                    this.localStore.setvalue('token',this.access_token);
        }
        console.log(res,'success');
        this.user_type=res.user.role;
        console.log(this.user_type);
        this.user_id=res.user.email;
        this.id=res.user.id;
        this.localStore.setvalue('id',this.id);
        this.localStore.setvalue('userid',this.user_id);
        this.localStore.setvalue('role',this.user_type);
        if(res.token && this.user_type=='student'){
           // this.navCtrl.push('ChoosePackagePage',{userid:this.user_id});
           this.navCtrl.push('TabPage',{userid:this.user_id});
        }else if(res.token && this.user_type=='tutor'){  
           this.navCtrl.push('TutorTabPage',{userid:this.user_id});
        }
      },error=>{
          console.log(error,'failed');
             let toast = this.toastCtrl.create({
                message: 'Invalid credentials',
                duration: 3000,
              position: 'bottom'
        });
        toast.present();
      })
    }else{
       let toast = this.toastCtrl.create({
    message: 'All fields are required',
    duration: 3000,
    position: 'bottom'
    });
    toast.present();
    }
  }
  getValue(){
    this.localStore.getvalue('token')
    // this.nativeStorage.getItem('token')
    .then(data=>{
      console.log(data);
      // this.loginform.controls['access_token'].setValue(data);
      this.access_token=data;
      this.localStore.delete('token');
    },error=>{
      console.log(error);
    })
  }
}
