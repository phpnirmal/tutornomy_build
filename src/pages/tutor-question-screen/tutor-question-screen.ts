import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tutor-question-screen',
  templateUrl: 'tutor-question-screen.html',
})
export class TutorQuestionScreenPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
