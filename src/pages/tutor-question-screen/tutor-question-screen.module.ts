import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorQuestionScreenPage } from './tutor-question-screen';

@NgModule({
  declarations: [
    TutorQuestionScreenPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorQuestionScreenPage),
  ],
})
export class TutorQuestionScreenPageModule {}
