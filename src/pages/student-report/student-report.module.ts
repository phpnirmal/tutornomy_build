import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentReportPage } from './student-report';

@NgModule({
  declarations: [
    StudentReportPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentReportPage),
  ],
})
export class StudentReportPageModule {}
