import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiServiceProvider} from '../../providers/api-service/api-service';
import {config } from '../../config/config';
import 'rxjs/add/operator/map';


@Injectable()
export class UserServiceProvider {

  constructor(public http: HttpClient,public apiSer:ApiServiceProvider) {
  }
   userSignup(data){
     console.log(data);
	return this.apiSer.post(config.UserSignup,data);
  }  
  userLogin(email,password){
  	return this.apiSer.loginPost(config.Login,email,password);
  }
  getprofileData(userid){
    console.log(userid);
  	// return this.apiSer.getNoheader(config.getStudentProfile+userid+'&&access_token='+token,{});
    return this.apiSer.get(config.getStudentProfile+userid,{});
  }
  createStudentProfile(profiledata){
    console.log(profiledata);
      return this.apiSer.post(config.create_student_profile,profiledata);
  }
  updateStudentProfile(profiledata,id){
    return this.apiSer.Put(config.update_student_profile+id,{});
  }
  createTutorProfile(profiledata){
    return this.apiSer.post(config.create_tutor_profile,profiledata);
  }
  retrieveTutorProfile(profileid){
    return this.apiSer.get(config.get_tutor_profile+profileid,{});
  }
  getCurrentUser(){
    return this.apiSer.get(config.get_current_user,{});
  }
  getSubjects(){
    return this.apiSer.get(config.get_subjects,{});
  }
  getAcademics(){
    return this.apiSer.get(config.get_academic_levels,{});
  }
}
