import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiServiceProvider} from '../../providers/api-service/api-service';
import {config } from '../../config/config';
import 'rxjs/add/operator/map';


@Injectable()
export class RequestProvider {

  constructor(public http: HttpClient,public apiSer:ApiServiceProvider) {
  }
  submitQuestion(param){
  	return this.apiSer.post(config.create_student_request,param);
  }
  getBackRequests(userid){
  	return this.apiSer.get(config.get_student_requests+userid,{})
  }
}
