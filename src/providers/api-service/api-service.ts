import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Http,Headers,RequestOptions,} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { NativeStorage } from '@ionic-native/native-storage';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';


@Injectable()
export class ApiServiceProvider {
  public credentials;
  public basic;
  public token;
  constructor(public http: HttpClient,public nativeStorage: NativeStorage,
                    public local:LocalStorageProvider) {
  }
    get(api,params) {
      this.getToken();
      console.log(api,params);
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':`Bearer ${this.token}`});
      return new Observable(observer => {
      return this.http.get(api,{headers:headers})
      .subscribe(response => {
      observer.next(response);
      });
      })
    }
    post(api,param) {
      console.log(api,param);
      let body = JSON.stringify(param);
      let headers = new Headers({ 'Content-Type': 'application/json'});
      let _options = new RequestOptions({ headers: headers });
      return this.http.post(api, param);
    }
       postnew(api,param) {
      console.log(api,param);
      let body = JSON.stringify(param);
      let headers = new HttpHeaders({ 'Content-Type': 'application/json',
    'Authorization':`Bearer ${this.token}`});
      // let _options = new RequestOptions({ headers: headers });
      return this.http.post(api, param,{headers:headers});
    }

    loginPost(api,email,password){
      this.credentials =email + ":" +password;
       this.basic = "Basic " + btoa(this.credentials);
      let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization':this.basic});
      let options = { headers: headers };
      console.log(options);
      return this.http.post(api,null,options);
    }
    Put(url,param){
      this.getToken();
      console.log(url,param);
      let body = JSON.stringify(param);
      let headers = new HttpHeaders({ 'Content-Type': 'application/json',
      'Authorization':`Bearer ${this.token}`});
      let options = { headers: headers};
      return this.http.put(url,null,options);
    }
      getToken(){
        this.local.getvalue('token')
         .then((token:any) => {
           this.token=token;
         })
      }
      getNoheader(api,params) {
        return new Observable(observer => {
                    return this.http.get(api,params)
                        .subscribe(response => {
                            observer.next(response);
                        });
        })
    }
}

    

