import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ValidationProvider {

  constructor(public http: HttpClient) {
  }
  emailValidation(control) {
		let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (control.value === null || typeof control.value === 'undefined') {
			return { invalidEmailAddress: true };
		} else if (control.value.match(re)) {
			return null;
		} else {
			return { invalidEmailAddress: true };
		}
	}
	passwordValidation(control) {
		if (control.value === null || typeof control.value === 'undefined') {
			return { invalidPassword: true };
		} else if (control.value.length > 5) {
			return null;
		} else {
			return { invalidPassword: true };
		}
	}

}
