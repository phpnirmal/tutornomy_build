import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiServiceProvider} from '../../providers/api-service/api-service';
import {config } from '../../config/config';
import 'rxjs/add/operator/map';

@Injectable()
export class TopupPlansProvider {

  constructor(public http: HttpClient,public apiSer:ApiServiceProvider) {
  }
getStudentPackages(){
    return this.apiSer.get(config.get_topup_plans,{});
  }
  createPlan(data){
  	return this.apiSer.postnew(config.create_topup_plan,data);
  }

}
