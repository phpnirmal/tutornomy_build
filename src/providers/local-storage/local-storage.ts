import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import {Platform } from 'ionic-angular';

declare const localStorage;
@Injectable()
export class LocalStorageProvider {

  constructor(public http: HttpClient,public nativeStorage: NativeStorage,
  		public platform:Platform) {
    console.log('Hello LocalStorageProvider Provider');
  }
   setvalue(key,val) {
         if (!this.platform.is('cordova')) {
            return new Promise((resolve) => {
                // this.local[key] = val;
                let value = JSON.stringify(val);
                localStorage.setItem(key, value);
                resolve(true);
            });
        } else {
            // this.cart.push(val);
            return this.nativeStorage.setItem(key,val);  
        }
      }
      getvalue(key){
       if (!this.platform.is('cordova')) {
            return new Promise((resolve) => {
               let item = localStorage.getItem(key);
               resolve(JSON.parse(item));
            });
        } else {
            return this.nativeStorage.getItem(key);
        }
      }
    delete(key) {
         return this.nativeStorage.remove(key);   
    }
    clear(){
      return this.nativeStorage.clear();
    }

}
