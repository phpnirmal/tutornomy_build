import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiServiceProvider} from '../../providers/api-service/api-service';
import {config } from '../../config/config';
import 'rxjs/add/operator/map';


@Injectable()
export class AccountProvider {

  constructor(public http: HttpClient,public apiSer:ApiServiceProvider) {
    console.log('Hello AccountProvider Provider');
  }
    getAccData(userid){
  	return this.apiSer.get(config.get_account_data+userid,{});
  }
  getTransaction(userid){
  	return this.apiSer.get(config.get_transaction_details+userid,{})
  }

}
